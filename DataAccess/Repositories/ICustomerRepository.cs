﻿using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repositories
{

    /// <summary>
    /// interface to CustomerRepository that has all the methods that the customerRepository should have and inherit from
    /// </summary>
    public interface  ICustomerRepository
    {

        public List<Customer> GetAllCustommers();

        public Customer GetOneCustomer(string id);
        public Customer GetByCustomerName(string name);

        public List<Customer> ReturnAPageOfCustomer(int limit, int offset);

        public bool AddNewCustomer(Customer customer);

        public bool UpdateCustomer(Customer customer);

        public List<CustumerCountry> NumberOfCustomer();

        public List<CustomerSpender> CustumerSpender();

        public List<CustomerGenre> CustomerGenre();




    }
}
