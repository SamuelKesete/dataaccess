﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repositories
{
    public class ConnectionStringHelper
    {

        public static string GetConnectionString()
        {

            //
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            // name of the Sql server
            connectionStringBuilder.DataSource = "N-SE-01-7294\\SQLEXPRESS";
            // name of the database -- using 
            connectionStringBuilder.InitialCatalog = "Chinook";
            // windows login 
            connectionStringBuilder.IntegratedSecurity = true;
            // return the connectionStringBuilder to use the class 
            connectionStringBuilder.Encrypt = false;
            return connectionStringBuilder.ConnectionString;

           

        }
    }
}
