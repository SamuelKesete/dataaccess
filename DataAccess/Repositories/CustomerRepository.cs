﻿using DataAccess.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Repositories
{


    /// <summary>
    /// Inherits from the interface ICustomerRepository
    /// Here we implement the actual methods that talks to the database and get something from it 
    /// </summary>
    /// <returns></returns>


    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// We get all the customers attribue of  CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from the customer table
        /// </summary>
        /// <returns></returns>
        public List<Customer> GetAllCustommers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email  FROM Customer ";
          
            try
            {
                // connect 
               
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //hundle result 
                                Customer temp = new Customer();
                                //Checks if the index is null
                                if (!reader.IsDBNull(0))
                                    temp.CustomerId = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    temp.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    temp.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    temp.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    temp.Phone = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    temp.Email = reader.GetString(6);
                                customerList.Add(temp);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return customerList;
        }


        /// <summary>
        /// Gets the specific customer by Id
        /// </summary>
        /// <returns></returns>
        public Customer GetOneCustomer(string id)
        {
            Customer customer = new Customer();
            string specefic = "SELECT CustomerId, FirstName, LastName, Country," +
                " PostalCode, Phone, Email FROM Customer WHERE CustomerId=@CustomerId";

            try
            {
                // connect 
                
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 
                    using (SqlCommand cmd = new SqlCommand(specefic, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //hundle result 


                                if (!reader.IsDBNull(0))
                                    customer.CustomerId = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    customer.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    customer.Phone = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    customer.Email = reader.GetString(6);


                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }

            return customer;

        }

        /// <summary>
        /// Gets the specific customer by name
        /// </summary>
        /// <returns></returns>
        public Customer GetByCustomerName(string name)
        {
            Customer CustomName = new Customer();
            string specefic = "SELECT CustomerId, FirstName, LastName, Country," +
                " PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @id";


            try
            {
                // connect 
                //using (SqlConnection conn = new SqlConnection().GetConnectionString()) ;

                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 
                    using (SqlCommand cmd = new SqlCommand(specefic, conn))
                    {
                        cmd.Parameters.AddWithValue("@id", name);
                        // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //hundle result 

                                if (!reader.IsDBNull(0))
                                    CustomName.CustomerId = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    CustomName.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    CustomName.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    CustomName.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    CustomName.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    CustomName.Phone = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    CustomName.Email = reader.GetString(6);


                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }

            return CustomName;
        }


        /// <summary>
        /// retunrs subset of customer with SQLLIMIT and offset 
        /// </summary>
        /// <param name="limit">deside a limit of customars with row for how many</param>
        /// <param name="offset">offset decides which index to starts from</param>
        /// <returns> retun a customer list</returns>
        public List<Customer> ReturnAPageOfCustomer(int limit, int offset)
        {
            List<Customer> customerListPge = new List<Customer>();
            string queryStr = "SELECT CustomerId, FirstName, LastName, Country," +
                " PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";

            try
            {
                // connect 
                

                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 
                    using (SqlCommand cmd = new SqlCommand(queryStr, conn))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit);
                        cmd.Parameters.AddWithValue("@OFFSET", offset);
                        // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //hundle result 
                                Customer temp = new Customer();

                                if (!reader.IsDBNull(0))
                                    temp.CustomerId = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    temp.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    temp.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    temp.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    temp.Phone = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    temp.Email = reader.GetString(6);
                                customerListPge.Add(temp);

                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }

            return customerListPge;

        }

        /// <summary>
        /// Creates a new customer by inserting a new one
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool seccces = false;
            string sql = "INSERT INTO Customer ( FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";

            try
            {


                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);

                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);

                        cmd.Parameters.AddWithValue("@Country", customer.Country);

                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);

                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);

                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        seccces = cmd.ExecuteNonQuery() > 0 ? true : false;


                    }
                }

            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }

            return seccces;

        }


        /// <summary>
        /// Updates a specif customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool result = false;
            string sql = "UPDATE Customer SET  FirstName=@FirstName, " +
                "LastName=@LastName, Country=@Country, PostalCode=@PostalCode, Phone=@Phone, Email=@Email WHERE CustomerId=@CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        result = cmd.ExecuteNonQuery() > 0 ? true : false;


                    }
                }

            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }

            return result;




        }

        /// <summary>
        /// Gets a customers in each county and order it by descending
        /// </summary>
        /// <returns></returns>

        public List<CustumerCountry> NumberOfCustomer()
        {

            List<CustumerCountry> countrylist = new List<CustumerCountry>();

            string sql = "Select Country, COUNT(Country) from Customer GROUP BY Country ORDER BY COUNT(Country) DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {


                        // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //hundle result 
                                CustumerCountry temp = new CustumerCountry();
                                if (!reader.IsDBNull(0))
                                    temp.Country = reader.GetString(0);
                                if (!reader.IsDBNull(1)) ;
                                temp.Number = reader.GetInt32(1);
                                countrylist.Add(temp);

                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }

            return countrylist;

        }


        /// <summary>
        /// Gets a customers who are the highest spenders via invoice table with innerjoin, group it and order it by descending
        /// </summary>
        /// <returns></returns>
        public List<CustomerSpender> CustumerSpender()
        {
            List<CustomerSpender> highestScore = new List<CustomerSpender>();
            string sql = "SELECT  Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM(Total) as total" +
            " FROM Customer " +
            "INNER JOIN Invoice ON  Customer.CustomerId = Invoice.CustomerId " +
            "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName " +
            "ORDER BY SUM(Total)  DESC; ";

            try
            {
                // connect 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                //hundle result 

                                CustomerSpender customerSpender = new CustomerSpender();
                                if (!reader.IsDBNull(0))
                                    customerSpender.ID = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    customerSpender.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    customerSpender.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                   customerSpender.Total = reader.GetDecimal(3);

                                highestScore.Add(customerSpender);



                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return highestScore;

        }

        /// <summary>
        /// Gets a top 3 who has the popular genre via tracks invoice, to find out the most popular tracks from Genre  
        /// In this part used the Join function from the related tables to find out the most popular track
        /// after getting the track we group it and order it by descending
        /// </summary>
        /// <returns>returns a mostTrack which contains top 3 who has the popular genre  </returns>
        public List<CustomerGenre> CustomerGenre()
        {

            List<CustomerGenre> mostTracks = new List<CustomerGenre>();
            string sql = " SELECT top(3)  " +
            "Customer.CustomerId, Customer.FirstName, Customer.LastName,  Genre.Name,COUNT(Genre.Name) as pupular " +
            "FROM Customer  " +
            "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId" +
            " INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
            "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
            "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
            "WHERE Customer.CustomerId = 13 " +
            "GROUP BY Customer.CustomerId,Customer.FirstName,Customer.LastName, Genre.Name " +
            "ORDER BY COUNT(Genre.Name) DESC;";

            try
            {
                // connect 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // oppen connection 
                    conn.Open();

                    // make a command 
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {


                            while (reader.Read())
                            {
                                //hundle result 

                                CustomerGenre customerGenre = new CustomerGenre();
                                if (!reader.IsDBNull(0))
                                    customerGenre.ID = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    customerGenre.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    customerGenre.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    customerGenre.GenreName = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    customerGenre.GenreCountName = reader.GetInt32(4);
                                mostTracks.Add(customerGenre);

                            }

                            if (mostTracks[0].GenreCountName == mostTracks[1].GenreCountName)
                            {
                                return mostTracks;
                            }
                            else
                            {
                                return new List<CustomerGenre>() { mostTracks[0] };

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return mostTracks;


        }


    }



}
