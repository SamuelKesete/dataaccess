﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{
    public class CustomerSpender
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Total { get; set; }

    }
}
