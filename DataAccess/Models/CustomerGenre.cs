﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{
   public class CustomerGenre
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string GenreName { get; set; }
        public int GenreCountName { get; set; }

    }
}
