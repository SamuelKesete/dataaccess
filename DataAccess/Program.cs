﻿using DataAccess.Models;
using DataAccess.Repositories;
using System;
using System.Collections.Generic;

namespace DataAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            // test reps

            TestSelectAll(repository );
            TestSelectOneCustomer(repository);
            TestNameGetBYCustomerName(repository);
            TestReturnAPageOfCustomer(repository);
            TestInsert(repository);
            TestUpdate(repository);
            TestCountrys(repository);
            TestCountrySpender(repository);
            TestCustomerGenre(repository);

        }






        //Get All Custommers Testmethod  
        static void TestSelectAll(ICustomerRepository repository)
        {
            PrintCustumers(repository.GetAllCustommers());

        }
        //Get one Custommer Testmethod 
        static void TestSelectOneCustomer(ICustomerRepository repository)
        {
            PrintCustumer(repository.GetOneCustomer("10"));
        }


        // Test method  for one specefic customer

        static void TestNameGetBYCustomerName(ICustomerRepository repository)
        {
            PrintCustumer(repository.GetByCustomerName("%Roberto"));
        }


        //Test method for returning a page of customer 
        static void TestReturnAPageOfCustomer(ICustomerRepository repository)
        {
            PrintCustumers(repository.ReturnAPageOfCustomer(3, 7));
        }

       

        // Test method for inserting a customer  

        static void TestInsert(ICustomerRepository repository)
        {

            Customer test = new Customer()
            {


                CustomerId = 1,
                FirstName = "Osko",
                LastName = "Anderson",
                Country = "Stockholm",
                PostalCode = "19550",
                Phone = "0700000000",
                Email = "example@example.com",

            };

            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("Insert Worked");

                PrintCustumer(repository.GetOneCustomer("3"));
            }
            else
            {
                Console.WriteLine("not working");
            }

        }


        // Test method for update  a customer 
        static void TestUpdate(ICustomerRepository repository)
        {
            Customer updtest = new Customer()
            {
                CustomerId = 1,
                FirstName = "Sami",
                LastName = "Samuel",
                Country = "Sigtuna",
                PostalCode = "01010",
                Phone = "070000000000000000",
                Email = "example@.commmmmmmmmmmmmmm",
            };

            if (repository.UpdateCustomer(updtest))
            {
                Console.WriteLine("Update work");
            }
            else
            {
                Console.WriteLine("update faile");
            }
        }

        // Test method for  customars country 
        static void TestCountrys(ICustomerRepository repository)
        {
            PrintCustumersCountry(repository.NumberOfCustomer());

        }


        // Test method for CustumerSpender 

        static void TestCountrySpender(ICustomerRepository repository)
        {
            PrintCustomarsSpender(repository.CustumerSpender());

        }


        // Test method for CustomerGenre
        static void TestCustomerGenre(ICustomerRepository repository)
        {
            PrintCustomersGenre(repository.CustomerGenre());

        }





        //**************************************************************//
        // Read all customers 
        // read a specefic customer by id 
        // read specefic customer by name 
        // return a page of customers limit and offset 
        // add a new customer 
        // update un existing customer
        //************************************************************//


        static void PrintCustumers(IEnumerable<Customer> customers)
        {
            foreach (Customer custumer in customers)
            {
                PrintCustumer(custumer);

            }

        }

        static void PrintCustumer(Customer custumer)
        {

            Console.WriteLine($"---{ custumer.CustomerId} {custumer.FirstName} {custumer.LastName} {custumer.Country} {custumer.PostalCode} {custumer.Phone} {custumer.Email}");


        }



        //***********************************************************************//
            // Return a number of customers in each country in desenting order high to low
        //***********************************************************************//



        public static void PrintCustumerCountry(CustumerCountry country)
        {
            Console.WriteLine($"---{country.Country} {" "} {country.Number}");
        }

        static void PrintCustumersCountry(IEnumerable<CustumerCountry> countrys)
        {
            foreach (CustumerCountry country in countrys)
            {
                PrintCustumerCountry(country);

            }


        }



        //********************************************************************************//
        //hights customers spenders total in invoce oredered descending
        //*******************************************************************************//

        public static void PrintCustomarSpender(CustomerSpender spender)
        {
            Console.WriteLine($"---{spender.ID} {" "} {spender.FirstName} {" "} {spender.LastName} {" "} {spender.Total}");

        }

        public static void PrintCustomarsSpender(IEnumerable<CustomerSpender> spenders)
        {
            foreach (CustomerSpender spender in spenders)
            {
                PrintCustomarSpender(spender);
            }
        }

        //****************************************************************//
        //  Top 3 who has the popular genre
        //****************************************************************//


        public static void PrintCustomerGenre(CustomerGenre genre)
        {
            Console.WriteLine($"---{genre.ID} {""} {genre.FirstName} {genre.LastName} {genre.GenreName} {genre.GenreCountName}");
        }


        public static void PrintCustomersGenre(IEnumerable<CustomerGenre> genres)
        {
            foreach (CustomerGenre genre in genres)
            {
                PrintCustomerGenre(genre);
            }
        }


    }
}
