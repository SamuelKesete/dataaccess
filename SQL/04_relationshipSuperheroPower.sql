
CREATE TABLE SuperheroPower (
	SuperheroID int,
	PowerID int,
	FOREIGN KEY (SuperheroID) REFERENCES Superhero(ID),
	FOREIGN KEY (PowerID) REFERENCES Powerr(PowerID),
	PRIMARY KEY (SuperheroID, PowerID)
)