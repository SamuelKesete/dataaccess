

CREATE TABLE Superhero (
	ID int IDENTITY(1,1) PRIMARY KEY,
	SuperheroNAME varchar(50) null,
	Alias varchar(50) null,
	Origin varchar(50) null
)

CREATE TABLE Assistant(
	AssistantID int IDENTITY(1,1) PRIMARY KEY,
	AssistantName varchar(50) null
)

CREATE TABLE Powerr(
	PowerID int IDENTITY(1,1) PRIMARY KEY,
	PowerName varchar(50) null,
	PowerPowerDescription varchar(50) null
)