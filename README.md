## Sql scripts and database-access
This project contains sql scripts and c# console application scripts which can be run to create a database,setup some tables in the database and create a relationship between them and do CRUD functionality.

## Table of Contents

- [Clone](#clone)
- [Usage](#usage)
- [Install](#Install)
- [Contributors](#contributors)


## Clone

You can clone it with this link link https://gitlab.com/SamuelKesete/dataaccess in your visual studio or

Alternativly open the cmdline 
```
$git clone https://gitlab.com/SamuelKesete/dataaccess

```
## Usage
The goal with these scripts that is to create database,create three tables Superhero,Assistants and Power.One Superhero can have multiple assistants and one assistant has one superhero.(one to many)One Superhero can have many powers and one power can be present on many Superheroes(many to many).

DataAccess scripts contains scripts that can connect to the existing database and can read data and manipulate it using the SQL Client library


## Install

To run the database access the user needs to install the Nuget-package Microsoft.Data.SqlClient. The user will also need SQL Express along with a database manager of their choice to run the SQL scripts in the SQL folder.

## Contributors

Samuel - @SamuelKesete

Usko - @usk1129 





